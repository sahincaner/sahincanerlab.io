# ci-cd-workshop

[![coverage report](https://gitlab.com/sahincaner/ci-cd-workshop/badges/master/coverage.svg)](https://gitlab.com/sahincaner/ci-cd-workshop/commits/master)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
